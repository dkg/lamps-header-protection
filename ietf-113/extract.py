#!/usr/bin/python3

import xml.etree.ElementTree as ET
import subprocess
import tempfile
import os.path
import re

outfile = 'ietf-113-lamps-header-protection.pdf'

tree = ET.parse('ietf-113-lamps-header-protection.svg')
omit_slide_nums = [1]

# see https://docs.python.org/3/library/xml.etree.elementtree.html#parsing-xml-with-namespaces
prefix_map={'inkscape': 'http://www.inkscape.org/namespaces/inkscape',
            'svg': 'http://www.w3.org/2000/svg',}
tmatcher = re.compile('^translate\(([-0-9.]*),([-0-9.]*)\)$')

content = tree.find('.//svg:g[@id="content"]', prefix_map)

pagenum = tree.find('.//svg:tspan[@id="pspan"]', prefix_map)

framelist = tree.findall('.//*[@inkscape:tiled-clone-of="#baseframe"]', prefix_map)

## do we set the viewbox attribute of the svg root node?

## or do we slide around the content layer?

frames = {}

for frame in framelist:
    frames[frame.attrib['id']] = frame


with tempfile.TemporaryDirectory() as workdir:
    outnames = []
    slidenum:int = 1
    for frameid in sorted(frames.keys()):
        print(f"processing {frameid}")
        # sort out the transform:
        transform = frames[frameid].attrib['transform']

        m = tmatcher.match(transform)
        if m is None:
            raise ValueError(f"transform is not what we expected: {transform}")
        (xstr, ystr) = m.groups()
        x = float(xstr)
        y = float(ystr)
        content.attrib['transform'] = f'translate({-x},{-y})'
        if slidenum in omit_slide_nums:
            pagenum.text = ''
        else:
            pagenum.text = f'{slidenum}/{len(frames)}'

        # write this out to a file, then invoke inkscape to generate the pdf:
        outf = tempfile.NamedTemporaryFile(suffix='.svg')
        tree.write(outf)
        outf.file.flush()
        outname = os.path.join(workdir, f'{frameid}.pdf')
        cmd = ['inkscape', '--export-filename', outname,
               '--export-overwrite', '--export-area-page', outf.name ]
        subprocess.call(cmd)
        outnames.append(outname)
        slidenum += 1

    print(f"assembling {outfile} from {outnames}")
    subprocess.call(['pdftk'] + outnames + ['cat', 'output', outfile])



