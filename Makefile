#!/usr/bin/make -f

DRAFT := draft-ietf-lamps-header-protection
OUTPUTS = $(DRAFT).xml $(DRAFT).txt $(DRAFT).html

TEST_VECTORS = $(shell ./generate-test-vectors list)
MESSAGES = $(addprefix header-protection.maildir/new/, $(addsuffix .eml, $(TEST_VECTORS)))
ASCII_ART = example-compose-interface.ascii-art example-reply-interface.ascii-art example-reply-interface-initial.ascii-art

PUBLIC_HOST ?= public-host.example
PUBLIC_PATH ?= /srv/www/header-protection.cmrg.net/web/

STAGING_PATH ?= /srv/header-protection/mail/staging/new/

# Programs
KRAMDOWN := kramdown-rfc2629
XML2RFC := xml2rfc

# Targets
all: $(OUTPUTS)

%.xml: %.mkd test-vectors.md $(ASCII_ART)
	$(KRAMDOWN) --v3 $< > $@.tmp
	mv $@.tmp $@

%.txt: %.xml
	$(XML2RFC) --v3 --text $< --out $@

%.html: %.xml
	$(XML2RFC) --v3 --html $< --out $@

alice.both.crt: alice.sign.crt alice.encrypt.crt
	cat $^ > $@

header-protection.maildir.zip: $(MESSAGES)
	zip -r $@ header-protection.maildir

header-protection.mbox: $(MESSAGES) alice.both.crt
	./generate-test-vectors mbox

message-list.md: $(MESSAGES) alice.both.crt
	./generate-test-vectors $@

test-vectors.md: $(MESSAGES) alice.both.crt
	./generate-test-vectors $@

latest.html: latest.md
	pandoc --standalone -o $@ $<

index.html: header-protection-test-vectors.md message-list.md
	cat header-protection-test-vectors.md message-list.md | pandoc --standalone -o $@ 

publish: index.html header-protection.maildir.zip header-protection.mbox
	rsync -avz --delete index.html .well-known ca.rsa.crt bob.pfx header-protection.mbox header-protection.maildir.zip header-protection.maildir $(PUBLIC_HOST):$(PUBLIC_PATH)

publish-staging: index.html header-protection.maildir.zip header-protection.mbox
	rsync -avz --delete header-protection.maildir/new/ $(PUBLIC_HOST):$(STAGING_PATH)

regenerate: alice.both.crt
	./generate-test-vectors

clean:
	rm -f $(OUTPUTS) *.tmp alice.both.crt test-vectors.md message-list.md index.html

check:
	codespell draft-ietf-lamps-header-protection.mkd
	mypy --strict generate-test-vectors
	mypy --strict tools/textconv-encrypted

.PHONY: clean all check publish regenerate publish-staging
